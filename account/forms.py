from django.forms import ModelForm
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model

User = get_user_model()

class RegisterForm(ModelForm):

	password1 = forms.CharField(label='Senha', widget=forms.PasswordInput)
	password2 = forms.CharField(label='Confirmação de Senha', widget=forms.PasswordInput)
	
	#Validação de senha
	def clean_password2(self):
		password1 = self.cleaned_data.get("password1")
		password2 = self.cleaned_data.get("password2")
		if password1 and password2 and password1 != password2:
			raise forms.ValidationError('As senhas introduzidas não são correspondentes')
			return password2

	def clean_cpf(self):
		return self.cleaned_data['cpf'] or None

	def clean_cnpj(self):
		return self.cleaned_data['cnpj'] or None

	#funcao para salvar o usuario com os dados provenientes da model
	def save(self, commit=True):
		#recorre a função original de save, o commit é false para não salvar, ou seja, apenas retornar o usuario
		#import pdb; pdb.set_trace()

		user = super(RegisterForm, self).save(commit=False)
		user.set_password(self.cleaned_data['password1'])
		if commit:
			user.save()
		return user

	class Meta:
		model = User
		fields = ["cpf","cnpj", "type_person", "email", "first_name", "last_name"]