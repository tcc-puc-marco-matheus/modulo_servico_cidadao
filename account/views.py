from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .forms import RegisterForm
from django.http import HttpResponse, HttpResponseRedirect
import requests
import json

def signup(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.save()
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect('account:stur_request') 
    else:
        form = RegisterForm()
    return render(request, 'signup.html', {'form': form})

@login_required
def stur_request(request):

    url = 'https://modulo-integracao-geral.herokuapp.com/stur/'

    if request.user.type_person == 'R':
        url += 'itr/' + request.user.cpf
    elif request.user.type_person == 'F':
        url += 'iptu/cpf/' + request.user.cpf
    else:
        url += 'iptu/cnpj/' + request.user.cnpj

    response = requests.get(url)
    
    if response.status_code == 200:
        args = {}

        args['first_name'] = request.user.first_name

        if request.user.type_person == 'R':
            args['type'] = 'R'
            args['tax'] = response.json()['ITR']
            args['key'] = response.json()['CPF']
        elif request.user.type_person == 'F':
            args['type'] = 'F'
            args['tax'] = response.json()['IPTU']
            args['key'] = response.json()['CPF']
        else:
            args['tax'] = response.json()['IPTU']
            args['key'] = response.json()['CNPJ']
            
        return render(request, 'taxes.html', args)
    return HttpResponse('Não foram encontrados registros no STUR')

@login_required
def signout(request):
    logout(request)
    # Redirect back to login page
    return HttpResponseRedirect('/')
