from django.contrib import admin
from django.urls import include, path
from django.contrib.auth.views import LoginView

from . import views

app_name = 'account'

urlpatterns = [
    path('cadastrar/', views.signup, name='signup'),
    path('entrar/', LoginView.as_view(), {'template_name': 'login.html'}, name='login'),
    path('sair/', views.signout, name='logout'),
    path('impostos/', views.stur_request, name='stur_request'),
]